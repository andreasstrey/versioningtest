package ch.strey.versioningtest;

import com.google.common.collect.ImmutableMap;
import java.util.Map;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
public class VersioningtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(VersioningtestApplication.class, args);
	}
        
        @RequestMapping(path = "/",  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        public Map<String, String> get() {
            return ImmutableMap.of("version", System.getProperty("application.version"));
        }
}
