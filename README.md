# POC: easy deployment of different versions in parallel

This is a simple POC to show how easy it is to deploy and run different versions in parallel. We are using a HA-Proxy as API-gateway and loadbalancer. The configuration is created by the service discovery consul. The predefined docker image for the haproxy uses consul-template to load the available services and creates on the fly the routing configuration.
The example spring boot app uses spring cloud consul to enable the registration via a simple annotation.

1. start consul: 

   `docker run --name=dev-consul -p 8500:8500 -e CONSUL_BIND_INTERFACE=eth0 consul`
   
2. start the HA-Proxy

   `docker run --name=haproxy --expose=80 --expose=1936 -p 8001:1936 -p 8000:80 -e CONSUL_CONNECT=172.17.0.2:8500 -e HAPROXY_DOMAIN=127.0.0.1.xip.io -e HAPROXY_STATS=true ciscocloud/haproxy-consul`

3. build the app

   `mvn clean install`

4. run the app multiple version in parallel

   `java -jar -Dapplication.version=v1 target/versioningtest-0.0.1-SNAPSHOT.jar --server.port=8080`
   `java -jar -Dapplication.version=v2 target/versioningtest-0.0.1-SNAPSHOT.jar --server.port=8081`
   `java -jar -Dapplication.version=v3 target/versioningtest-0.0.1-SNAPSHOT.jar --server.port=8082`
   `java -jar -Dapplication.version=v4 target/versioningtest-0.0.1-SNAPSHOT.jar --server.port=8083`

5. request the service

   `curl http://versioningtestv1.127.0.0.1.xip.io:8000/`
   `curl http://versioningtestv2.127.0.0.1.xip.io:8000/`
   `curl http://versioningtestv3.127.0.0.1.xip.io:8000/`
   `curl http://versioningtestv4.127.0.0.1.xip.io:8000/`

The example application response contains the value of the start parameter **application.version**
